//
//  HomeFactory.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import UIKit
class HomeFactory {
    
    static func createHomeViewController() -> UIViewController{
        
        let homeViewController = HomeViewController()
        
        let apiClient = ApiFactory.getApiClient()
        let homeApi = HomeApi(apiClient: apiClient)
        let homeManager = HomeManager(api: homeApi)
        let homeViewModel = HomeViewModel(manager: homeManager, viewController: homeViewController)
        
        homeViewController.viewModel = homeViewModel
        
        return homeViewController
    }
    
}
