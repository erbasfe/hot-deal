//
//  HotDealCellViewModel.swift
//  HotDeals
//
//  Created by Feridun Erbas on 19.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
protocol HotDealCellViewModelProtocol: class {
    func getTitle() -> String?
    func getBody() -> String?
}
class HotDealCellViewModel {
    private var title: String?
    private var expirationDate: Date?
    
    init(model: HotDeal) {
        title = model.title
        expirationDate = getDate(stringDate: model.expirationDate)
    }
    
    init() {
        
    }
    
    func getDate(stringDate: String?) -> Date?{
        guard let stringDate = stringDate else {return nil}
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return formatter.date(from: stringDate)
    }
}

extension HotDealCellViewModel: HotDealCellViewModelProtocol{
    
    func getTitle() -> String?{
        return title
    }
    func getBody() -> String? {
        guard let expirationDate = expirationDate else{
            return nil
        }
        
        let components = Calendar.current.dateComponents([.year,.month,.day, .hour,.minute,.second], from: Date(), to: expirationDate)
        var stringComponents: [String] = []
        
        if let year = components.year, year > 0 {
            stringComponents.append("\(year) Years")
        }
        if let month = components.month, month > 0  {
            stringComponents.append("\(month) Months")
        }
        if let day = components.day, day > 0  {
            stringComponents.append("\(day) Days")
        }
        if let hour = components.hour, hour > 0  {
            stringComponents.append("\(hour) Hours")
        }
        if let minute = components.minute, minute > 0  {
            stringComponents.append("\(minute) Minutes")
        }
        if let second = components.second, second > 0  {
            stringComponents.append("\(second) Seconds")
        }
        
        if stringComponents.count == 0 {
            return "Expired"
        }
        
        return stringComponents.joined(separator: ", ") + " remaining"
        
    }
    
}
