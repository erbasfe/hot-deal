//
//  GenericBannerCellViewModel.swift
//  HotDeals
//
//  Created by Feridun Erbas on 19.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

protocol GenericBannerCellViewModelProtocol: class {
    func getImageUrl() -> URL?
}

class GenericBannerCellViewModel {
    
    private var imageUrl: String?
    
    init(model: GenericBanner) {
        imageUrl = model.image?.url
    }
}

extension GenericBannerCellViewModel: GenericBannerCellViewModelProtocol{
    func getImageUrl() -> URL? {
        guard let imageUrl = self.imageUrl else{return nil}
        return URL.init(string: imageUrl)
    }
}
