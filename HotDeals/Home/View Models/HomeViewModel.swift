//
//  HomeViewModel.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

protocol HomeViewModelProtocol: BaseViewModelProtocol {
    func numberOfRows() -> Int
    func willDisplayCellAt(index: Int)
    func cellNamesToRegister() -> [String]
    func cellType(for index: Int) -> HomeCellType
    func cellViewModel(for index: Int) -> Any?
    func estimatedHeightForRow(at index: Int) -> TableViewHeightType
    func heightForRowAt(index: Int) -> TableViewHeightType
}

class HomeViewModel {
    
    private var dataSource: [Any] = []
    private var lastSuccessfulRetreivedPage = 0
    
    weak var viewController: HomeViewControllerProtocol?
    var manager: HomeManagerProtocol?
    
    init(manager: HomeManagerProtocol, viewController: HomeViewControllerProtocol?) {
        self.manager = manager
        self.viewController = viewController
    }
    
    private func getNextFeed(){
        let page = self.lastSuccessfulRetreivedPage
        manager?.getFeed(page: page, completion: { [weak self] (banners, deals, error) in
            if let error = error{
                self?.handleError(error: error)
                return
            }
            
            if let newValues = self?.combineArrays(array1: banners, array2: deals), newValues.count > 0{
                self?.handleNewData(array: newValues)
            }
            self?.lastSuccessfulRetreivedPage += 1
        })
    }
    
    private func combineArrays(array1: [Any], array2: [Any]) -> [Any]{
        var result: [Any] = []
        let minimumSize = min(array1.count, array2.count)
        for i in 0..<minimumSize {
            result.append(array1[i])
            result.append(array2[i])
        }
        for i in minimumSize..<array1.count {
            result.append(array1[i])
        }
        for i in minimumSize..<array2.count {
            result.append(array2[i])
        }
        return result
    }
    
    private func handleError(error: String){
        viewController?.showAlert(title: "Error", message: error)
    }
    
    private func handleNewData(array: [Any]){
        self.dataSource += array
        self.viewController?.reloadTableView()
    }
}

extension HomeViewModel: HomeViewModelProtocol{
    func estimatedHeightForRow(at index: Int) -> TableViewHeightType{
        let type = cellType(for: index)
        switch type {
        case .genericBanner:
            let banner = self.dataSource[index] as! GenericBanner
            guard let width = banner.image?.width, let height = banner.image?.height, let containerWidth = self.viewController?.tableViewFrameWidth() else {
                return .automatic
            }
            return .value(constant: containerWidth * Float(height) / Float(width))
        default:
            return .automatic
        }
        
    }
    
    func heightForRowAt(index: Int)  -> TableViewHeightType{
        return .automatic
    }
    
    
    func cellNamesToRegister() -> [String] {
        let allcases = HomeCellType.all
        return allcases.map({ (element) -> String in
            return element.rawValue
        })
    }
    
    func numberOfRows() -> Int {
        return self.dataSource.count
    }
    
    func willDisplayCellAt(index: Int) {
        if index == self.dataSource.count - 1 {
            self.getNextFeed()
        }
    }
    
    func viewDidLoad() {
        viewController?.initializeTableView()
        self.getNextFeed()
    }
    
    func cellType(for index: Int) -> HomeCellType {
        let row = self.dataSource[index]
        if let _ = row as? GenericBanner{
            return .genericBanner
        }
        return .hotDeal
    }
    
    func cellViewModel(for index: Int) -> Any? {
        if let genericBanner =  self.dataSource[index] as? GenericBanner{
            return GenericBannerCellViewModel(model: genericBanner)
        }else if let hotDeal = self.dataSource[index] as? HotDeal{
            return HotDealCellViewModel(model: hotDeal)
        }
        return nil
    }
    
}
