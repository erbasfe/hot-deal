//
//  HotDealCell.swift
//  HotDeals
//
//  Created by Feridun Erbas on 19.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import UIKit

class HotDealCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelBody: UILabel!
    var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func configure(viewModel: Any?) {
        
        guard let viewModel = viewModel as? HotDealCellViewModelProtocol else{
            return
        }
        
        self.labelBody.text = viewModel.getBody()
        self.labelTitle.text = viewModel.getTitle()
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (_) in
            self.labelBody.text = viewModel.getBody()
        }
    }
   
}
