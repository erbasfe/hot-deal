//
//  GenericBannerCell.swift
//  HotDeals
//
//  Created by Feridun Erbas on 19.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import UIKit
import Kingfisher

class GenericBannerCell: UITableViewCell {

    @IBOutlet weak var imageViewBanner: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func configure(viewModel: Any?) {
        
        guard let viewModel = viewModel as? GenericBannerCellViewModelProtocol else{
            return
        }
        self.imageViewBanner.kf.setImage(with: viewModel.getImageUrl())
    }
    
}
