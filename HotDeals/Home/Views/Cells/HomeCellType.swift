//
//  HomeCellType.swift
//  HotDeals
//
//  Created by Feridun Erbas on 19.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
enum HomeCellType: String, CaseIterable{
    case hotDeal = "HotDealCell"
    case genericBanner = "GenericBannerCell"
    
    static var all: [HomeCellType] {
        return [.genericBanner,.hotDeal]
    }
    
}
