//
//  BannerImage.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
class BannerImage: Codable {
    
    var width: Int?
    var height: Int?
    var url: String?
    
}
