//
//  HomeManager.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

typealias GetHomeFeedCompletion = (_ banners: [GenericBanner], _ hotDeals: [HotDeal], _ error: String?) -> ()

protocol HomeManagerProtocol: class {
    func getFeed(page: Int, completion: @escaping GetHomeFeedCompletion)
}

class HomeManager : HomeManagerProtocol {
    
    var api: HomeApiProtocol?
    init(api: HomeApiProtocol) {
        self.api = api
    }
    
    func getFeed(page: Int,completion: @escaping GetHomeFeedCompletion) {
        guard let api = api else{
            
            completion([],[],"unable to create api")
            return
        }
        api.getHomeFeed(page: page) { (response, error) in
            let banners = response?.banners ?? []
            let deals = response?.hotDeals ?? []
            completion(banners,deals,error?.description)
        }
    }
}
