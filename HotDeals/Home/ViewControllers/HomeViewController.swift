//
//  HomeViewController.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import UIKit

protocol HomeViewControllerProtocol: class {
    func reloadTableView()
    func initializeTableView()
    func tableViewFrameWidth() -> Float
    func showAlert(title: String, message: String)
}

class HomeViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var viewModel: HomeViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.viewDidLoad()
    }

}

extension HomeViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let heightType = viewModel?.heightForRowAt(index: indexPath.row) else{
            return UITableView.automaticDimension
        }
        switch heightType {
        case .automatic:
            return UITableView.automaticDimension
        case .value(let value):
            return CGFloat(value)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let heightType = viewModel?.estimatedHeightForRow(at: indexPath.row) else{
            return UITableView.automaticDimension
        }
        switch heightType {
        case .automatic:
            return UITableView.automaticDimension
        case .value(let value):
            return CGFloat(value)
        }
    }
}

extension HomeViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = viewModel?.cellType(for: indexPath.row) else{return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: cellType.rawValue, for: indexPath)
        let cellViewModel = viewModel?.cellViewModel(for: indexPath.row)
        cell.configure(viewModel: cellViewModel)
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel?.willDisplayCellAt(index: indexPath.row)
    }
}

extension HomeViewController: HomeViewControllerProtocol{
    func showAlert(title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction.init(title: "Ok", style: .cancel, handler: nil))
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func tableViewFrameWidth() -> Float {
        return Float(tableView.frame.width)
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    func initializeTableView() {
        guard let cellTypeNames = viewModel?.cellNamesToRegister() else{return}
        for typeName in cellTypeNames{
            let nib = UINib(nibName: typeName, bundle: nil)
            self.tableView.register(nib, forCellReuseIdentifier: typeName)
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.tableFooterView = UIView()
        
    }
}
