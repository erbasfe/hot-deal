//
//  HomeApi.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

typealias HomeFeedResultCompletion = (_ result: Feedresponse?, _ error: NetworkError?) -> Void
protocol HomeApiProtocol: class {
    func getHomeFeed(page: Int, completion: @escaping HomeFeedResultCompletion)
}

class HomeApi: HomeApiProtocol {
    var apiClient: ApiClient?
    
    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }
    
    func getHomeFeed(page: Int, completion: @escaping HomeFeedResultCompletion) {
        
        guard let apiClient = apiClient else {return}
        let realtiveUrl = UrlType.feed(page: page).realtiveUrlString
        let model = ApiRequest(method: .get, relativeUrl: realtiveUrl, parameters: [:])
        
        apiClient.request(model: model) { (data, error) in
            
            if let data = data{
                if let result = try? JSONDecoder().decode(Feedresponse.self, from: data){
                    completion(result, nil)
                }else{
                    completion(nil, NetworkError(description: "Unable to parse result data"))
                }
            }else if let error = error {
                completion(nil, error)
            }else{
                completion(nil, NetworkError(description: "Fatal issue: No data or error produced by apiClient"))
            }
        }
    }
    
    private enum UrlType{
        case feed(page: Int)
        
        var realtiveUrlString: String{
            switch self {
            case .feed(let page):
                return "campaigns/\(page)"
            }
        }
        
    }
}
