//
//  FeedResponse.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
class Feedresponse: Codable {
    
    var hotDeals: [HotDeal]?
    var banners: [GenericBanner]?
    
    
}
