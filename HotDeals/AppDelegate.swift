//
//  AppDelegate.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        
        changeRootViewController(vc: HomeFactory.createHomeViewController())
        
        return true
    }
    
    /**
     
     This method initializes the window property if not initialized and changes its root view controller to new value
     
     - Parameter vc: New view controller to become root view controller
     
     */

    private func changeRootViewController(vc: UIViewController){
        if self.window == nil{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.makeKeyAndVisible()
        }
        self.window?.rootViewController = vc
        
    }
    
    /**
     
     This method uses **UIApplication.shared.delegate** and changes the delegates root view controller
     
     - Parameter vc: New view controller to become root view controller
     
     - Returns: If the method was successfully changed the root view controller.
     */
    
    @discardableResult
    static func setRootViewController(vc: UIViewController) -> Bool{
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        delegate.changeRootViewController(vc: vc)
        return true
    }

}

