//
//  HttpMethod.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
enum HttpMethod {
    
    case get
    case post
    case put
    case delete
    
}
