//
//  ApiClient.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
typealias ApiRequestCompletion = ((_ data: Data?, _ error: NetworkError?) -> Void)
protocol ApiClient: class {
    func request(model: ApiRequest, completion: @escaping ApiRequestCompletion)
}
