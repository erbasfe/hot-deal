//
//  AlamofireApiClient.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireApiClient: ApiClient {
    
    func request(model: ApiRequest, completion: @escaping ApiRequestCompletion) {
        
        let translation = translate(method: model.method)
        
        Alamofire.request(model.url, method: translation.method, parameters: model.parameters, encoding: translation.encoding, headers: model.headers).responseData { (response) in
            var error: NetworkError?
            if let description = response.error?.localizedDescription{
                error = NetworkError(description: description)
            }
            
            completion(response.data, error)
        }
        
    }
    
    private func translate(method: HttpMethod) -> (method: HTTPMethod, encoding: ParameterEncoding){
        switch method {
        case .get:
            return (.get, URLEncoding(destination: .methodDependent))
        case .post:
            return (.post, JSONEncoding.default)
        case .delete:
            return (.delete, URLEncoding(destination: .methodDependent))
        case .put:
            return (.put, JSONEncoding.default)
        }
    }
    
}
