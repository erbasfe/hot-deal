//
//  ApiRequest.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

class ApiRequest {
    
    var method: HttpMethod
    var url: String
    var parameters: [String: Any]
    var headers: [String: String] = [:]
    
    

    /**
     Initializes a new ApiRequest instance with the provided values.
     
     - Parameters:
     - method: Http method to be used for the request
     - url: Full endpoint url
     - \parameters: Parameters to be used when making the request
     
     - Returns: An ApiRequest Instance.
     */
    
    init(method: HttpMethod, url: String, parameters: [String: Any] ) {
        self.method = method
        self.url = url
        self.parameters = parameters
    }
    
    /**
     Initializes a new ApiRequest instance with the provided values. Uses Configurations.apiUrl to obtain full url
     
     - Parameters:
     - method: Http method to be used for the request
     - relativeUrl: relative url to the endpoint
     - \parameters: Parameters to be used when making the request
     
     - Returns: An ApiRequest Instance.
     */
    
    convenience init(method: HttpMethod, relativeUrl: String, parameters: [String: Any] ) {
        self.init(method: method, url: Configurations.apiUrl + relativeUrl, parameters: parameters)
    }
    
}
