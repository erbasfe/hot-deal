//
//  ApiFactory.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation
class ApiFactory {
    
    static func getApiClient() -> ApiClient{
        let apiClient = AlamofireApiClient()
        return apiClient
    }
    
}
