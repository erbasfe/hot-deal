//
//  BaseViewModel.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

protocol BaseViewModelProtocol: class {
    func viewDidLoad()
    
}

extension BaseViewModelProtocol{
    func viewWillAppear(){}
    func viewDidAppear(){}
    func viewWillDisappear(){}
}

class BaseViewModel: BaseViewModelProtocol {
    
    func viewDidLoad() {
        
    }
    
}
