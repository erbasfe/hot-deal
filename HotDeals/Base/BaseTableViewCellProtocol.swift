//
//  BaseTableViewCellProtocol.swift
//  HotDeals
//
//  Created by Feridun Erbas on 19.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

protocol BaseTableViewCellProtocol {
    
}

extension BaseTableViewCellProtocol{
    func configure(viewModel: Any?) {}
}

