//
//  Configurations.swift
//  HotDeals
//
//  Created by Feridun Erbas on 18.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import Foundation

class Configurations {
    
    private var dictionary: NSMutableDictionary
    
    static private let shared = Configurations()
    
    private init(){
        let file = Bundle.main.path(forResource: "Configurations", ofType: "plist")!
        self.dictionary = NSMutableDictionary(contentsOfFile: file)!
    }
    
    static var apiUrl: String{
        return shared.dictionary["apiUrl"] as! String
    }
    
}
