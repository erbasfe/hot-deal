//
//  HotDealCellViewModelTest.swift
//  HotDealsTests
//
//  Created by Feridun Erbas on 20.05.2019.
//  Copyright © 2019 Feridun Erbas. All rights reserved.
//

import XCTest
@testable import HotDeals

class HotDealCellViewModelTest: XCTestCase {
    
    func testDateFormatter() {
  
        let viewModel = HotDealCellViewModel()
        let validDate = "2019-01-01T11:30:00+03:00"

        // Valid case
        XCTAssert(viewModel.getDate(stringDate: validDate) != nil)
        
        // Invalid cases
        XCTAssert(viewModel.getDate(stringDate: "ds") == nil)
        XCTAssert(viewModel.getDate(stringDate: "2019-01-01") == nil)
        XCTAssert(viewModel.getDate(stringDate: "2019-01-01T11:30:00") == nil)
        
    }
    
}
